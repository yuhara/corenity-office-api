<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// AccountCOA ROUTES
Route::get('AccountCOA/all','AccountCOAController@index');
Route::get('AccountCOA/{stCOAid}','AccountCOAController@getbyprimarykey');
Route::post('AccountCOA/store/','AccountCOAController@store');
Route::post('AccountCOA/update/{stCOAid}','AccountCOAController@update');
Route::post('AccountCOA/delete/{stCOAid}','AccountCOAController@delete');

// AccountCOATr ROUTES
Route::get('AccountCOATr/all','AccountCOATrController@index');
Route::get('AccountCOATr/{stCOATrID}','AccountCOATrController@getbyprimarykey');
Route::post('AccountCOATr/store/','AccountCOATrController@store');
Route::post('AccountCOATr/update/{stCOATrID}','AccountCOATrController@update');
Route::post('AccountCOATr/delete/{stCOATrID}','AccountCOATrController@delete');

// AccountCOAType ROUTES
Route::get('AccountCOAType/all','AccountCOATypeController@index');
Route::get('AccountCOAType/{stCOATypeID}','AccountCOATypeController@getbyprimarykey');
Route::post('AccountCOAType/store/','AccountCOATypeController@store');
Route::post('AccountCOAType/update/{stCOATypeID}','AccountCOATypeController@update');
Route::post('AccountCOAType/delete/{stCOATypeID}','AccountCOATypeController@delete');
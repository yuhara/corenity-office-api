<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCOATr extends Model
{
    protected $connection = 'mysql';

    protected $table = 'tblaccCOATr';

    public $timestamps = false;

    public $incrementing = false;
}

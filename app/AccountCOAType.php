<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCOAType extends Model
{
    protected $connection = 'mysql';

    protected $table = 'tblaccCOAType';

    public $timestamps = false;

    public $incrementing = false;
}

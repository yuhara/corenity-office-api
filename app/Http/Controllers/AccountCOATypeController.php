<?php

namespace App\Http\Controllers;

use App\AccountCOA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountCOATypeController extends Controller
{
    public function index()
    {
        $data = AccountCOATypeController::all();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }
    
    public function getbyprimarykey($stCOATypeID)
    {
        $dbcon = DB::connection('mysql');
        $data = $dbcon->table('tblaccCOAType')
        ->where('stCOATypeID',$stCOATypeID)->get();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function store(Request $request)
    {
        $dbcon = DB::connection('mysql');
        $bodyRequest = json_decode($request->getContent(),true);
        $biO2id = $bodyRequest['biO2id'];
        $stCOATypeID = $bodyRequest['stCOATypeID'];
        $stCOATypeName = $bodyRequest['stCOAName'];
        
        $data = new AccountCOATypeController();
        $data->biO2id = $biO2id;
        $data->stCOATypeID = $stCOATypeID;
        $data->stCOATypeName = $stCOAName;

        if($data->save()){
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['value'] = $data;
            return response($res);
        }else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function update(Request $request, $stCOATypeID)
    {
        $bodyRequest = json_decode($request->getContent(),true);
        $rowaffected = AccountCOAController::where('stCOATypeID', $stCOATypeID)->update($bodyRequest);

        $res['error'] = false;
        $res["values"] = ['rows_affected'=>$rowaffected];
        return response($res);
    }

    public function delete($stCOATypeID)
    {
        $data = AccountCOAController::where('stCOATypeID', '=', $stCOATypeID)->delete();

        $res['error'] = false;
        $res['message'] = "Success !";
        return response($res);
    }
}

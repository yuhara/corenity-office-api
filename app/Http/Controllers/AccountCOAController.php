<?php

namespace App\Http\Controllers;

use App\AccountCOA;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountCOAController extends Controller
{
    public function index()
    {
        $data = AccountCOAController::all();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }
    
    public function getbyprimarykey($stCOAid)
    {
        $dbcon = DB::connection('mysql');
        $data = $dbcon->table('tblaccCOA')
        ->where('stCOAid',$stCOAid)->get();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function store(Request $request)
    {
        $dbcon = DB::connection('mysql');
        $bodyRequest = json_decode($request->getContent(),true);
        $biO2id = $bodyRequest['biO2id'];
        $stCOATypeID = $bodyRequest['stCOATypeID'];
        $stCOAid = $bodyRequest['stCOAid'];
        $stCOAName = $bodyRequest['stCOAName'];
        $stCOAGroup = $bodyRequest['stCOAGroup'];
        $iCOALevel = $bodyRequest['iCOALevel'];
        $tiCOASummDetail = $bodyRequest['tiCOASummDetail'];
        $btHeader = $bodyRequest['btHeader'];
        $btProject = $bodyRequest['btProject'];
        $stDrCr = $bodyRequest['stDrCr'];
        $btActive = $bodyRequest['btActive'];
        $iSort = $bodyRequest['iSort'];
        $iStatus = $bodyRequest['iStatus'];
        $stInputBy = $bodyRequest['stInputBy'];
        $dtInput = $bodyRequest['dtInput'];
        $stUpdateBy = $bodyRequest['stUpdateBy'];
        $dtUpdate = $bodyRequest['dtUpdate'];
        $stSubmitBy = $bodyRequest['stSubmitBy'];
        $dtSubmit = $bodyRequest['dtSubmit'];
        $stCheckBy = $bodyRequest['stCheckBy'];
        $dtCheck = $bodyRequest['dtCheck'];
        $stApproveBy = $bodyRequest['stApproveBy'];
        $dtApprove = $bodyRequest['dtApprove'];
        
        $data = new AccountCOAController();
        $data->biO2id = $biO2id;
        $data->stCOATypeID = $stCOATypeID;
        $data->stCOAid = $stCOAid;
        $data->stCOAName = $stCOAName;
        $data->stCOAGroup = $stCOAGroup;
        $data->iCOALevel = $iCOALevel;
        $data->tiCOASummDetail = $tiCOASummDetail;
        $data->btHeader = $btHeader;
        $data->btProject = $btProject;
        $data->stDrCr = $stDrCr;
        $data->btActive = $btActive;
        $data->iSort = $iSort;
        $data->iStatus = $iStatus;
        $data->stInputBy = $stInputBy;
        $data->dtInput = $dtInput;
        $data->stUpdateBy = $stUpdateBy;
        $data->dtUpdate = $dtUpdate;
        $data->stSubmitBy = $stSubmitBy;
        $data->dtSubmit = $dtSubmit;
        $data->stCheckBy = $stCheckBy;
        $data->dtCheck = $dtCheck;
        $data->stApproveBy = $stApproveBy;
        $data->dtApprove = $dtApprove;

        if($data->save()){
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['value'] = $data;
            return response($res);
        }else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function update(Request $request, $stCOAid)
    {
        $bodyRequest = json_decode($request->getContent(),true);
        $rowaffected = AccountCOAController::where('stCOAid', $stCOAid)->update($bodyRequest);

        $res['error'] = false;
        $res["values"] = ['rows_affected'=>$rowaffected];
        return response($res);
    }

    public function delete($stCOAid)
    {
        $data = AccountCOAController::where('stCOAid', '=', $stCOAid)->delete();

        $res['error'] = false;
        $res['message'] = "Success !";
        return response($res);
    }
}

<?php

namespace App\Http\Controllers;

use App\AccountCOATr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountCOATrController extends Controller
{
    public function index()
    {
        $data = AccountCOATrController::all();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }
    
    public function getbyprimarykey($stCOATrID)
    {
        $dbcon = DB::connection('mysql');
        $data = $dbcon->table('tblaccCOATr')
        ->where('stCOATrID',$stCOATrID)->get();

        if (count($data) > 0) {
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['values'] = $data;
            return response($res);
        }
        else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function store(Request $request)
    {
        $dbcon = DB::connection('mysql');
        $bodyRequest = json_decode($request->getContent(),true);
        $biO2id = $bodyRequest['biO2id'];
        $stCOAid = $bodyRequest['stCOAid'];
        $stCOATrID = $bodyRequest['stCOATrID'];
        $stCOATrName = $bodyRequest['stCOATrName'];
        $btActive = $bodyRequest['btActive'];
        $iSort = $bodyRequest['iSort'];
        $iStatus = $bodyRequest['iStatus'];
        $stInputBy = $bodyRequest['stInputBy'];
        $dtInput = $bodyRequest['dtInput'];
        $stUpdateBy = $bodyRequest['stUpdateBy'];
        $dtUpdate = $bodyRequest['dtUpdate'];
        $stSubmitBy = $bodyRequest['stSubmitBy'];
        $dtSubmit = $bodyRequest['dtSubmit'];
        $stCheckBy = $bodyRequest['stCheckBy'];
        $dtCheck = $bodyRequest['dtCheck'];
        $stApproveBy = $bodyRequest['stApproveBy'];
        $dtApprove = $bodyRequest['dtApprove'];

        $data = new AccountCOATrController();
        $data->biO2id = $biO2id;
        $data->stCOAid = $stCOAid;
        $data->stCOATrID = $stCOATrID;
        $data->stCOATrName = $stCOATrName;
        $data->btActive = $btActive;
        $data->iSort = $iSort;
        $data->iStatus = $iStatus;
        $data->stInputBy = $stInputBy;
        $data->dtInput = $dtInput;
        $data->stUpdateBy = $stUpdateBy;
        $data->dtUpdate = $dtUpdate;
        $data->stSubmitBy = $stSubmitBy;
        $data->dtSubmit = $dtSubmit;
        $data->stCheckBy = $stCheckBy;
        $data->dtCheck = $dtCheck;
        $data->stApproveBy = $stApproveBy;
        $data->dtApprove = $dtApprove;

        if($data->save()){
            $res['error'] = false;
            $res['message'] = "Success ! ";
            $res['value'] = $data;
            return response($res);
        }else {
            $res['error'] = true;
            $res['message'] = "The Data is empty !";
            return response($res);
        }
    }

    public function update(Request $request, $stCOATrID)
    {
        $bodyRequest = json_decode($request->getContent(),true);
        $rowaffected = AccountCOATrController::where('stCOATrID', $stCOATrID)->update($bodyRequest);

        $res['error'] = false;
        $res["values"] = ['rows_affected'=>$rowaffected];
        return response($res);
    }

    public function delete($stCOATrID)
    {
        $data = AccountCOATrController::where('stCOATrID', '=', $stCOATrID)->delete();

        $res['error'] = false;
        $res['message'] = "Success !";
        return response($res);
    }
}

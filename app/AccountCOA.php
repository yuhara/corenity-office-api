<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCOA extends Model
{
    protected $connection = 'mysql';

    protected $table = 'tblaccCOA';

    public $timestamps = false;

    public $incrementing = false;
}
